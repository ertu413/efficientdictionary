function WordNode() {
    this.word = null;
    this.searchedCount = 1;
    this.turengNode = null;
    this.sesliSozlukNode = null;
    this.dictionaryNode = null;
    this.savedMeanings = [];
    this.isHighlightActive = true;
}

function EfficientDictionaryDB() {
    this.db = null;
    this.dbAndTableName = null;
}

EfficientDictionaryDB.prototype.openDB = function () {

    var me = this;

    if (!("indexedDB" in window)) {
        console.log("IndexedDB not supported!");
    }

    me.dbAndTableName = "dictionary";

    var openRequest = indexedDB.open(me.dbAndTableName, 1);

    openRequest.onupgradeneeded = function (e) {
        var thisDB = e.target.result;

        if (!thisDB.objectStoreNames.contains(me.dbAndTableName)) {
            thisDB.createObjectStore(me.dbAndTableName, {keyPath: "word"});
        }
    };

    openRequest.onsuccess = function (e) {

        console.log("Running onsuccess");

        me.db = e.target.result;
    };

    openRequest.onerror = function (e) {
        console.log("Could NOT Opened DB!");
    };
};

EfficientDictionaryDB.prototype.closeDB = function () {

    var me = this;

    me.db.close();
};


EfficientDictionaryDB.prototype.addWordNode = function (wordNode) {

    var me = this;

    var transaction = me.db.transaction([me.dbAndTableName], "readwrite");
    var store = transaction.objectStore(me.dbAndTableName);

    var request = store.add(wordNode);

    request.onerror = function (e) {
        console.log("Could not add word !");
    };

    request.onsuccess = function (e) {
        console.log("Word added successfully!");
    }
};

//TODO maybe update searchedCount over here after retriving!!
EfficientDictionaryDB.prototype.getWordNode = function (word) {

    var me = this;

    var transaction = me.db.transaction([me.dbAndTableName], "readwrite");
    var store = transaction.objectStore(me.dbAndTableName);

    var request = store.get(word);

    request.onsuccess = function (e) {

        console.log("Word retrieved successfully!");

        return e.target.result;
    };

    request.onerror = function (e) {
        console.log("Could not find word !");
    };
};

EfficientDictionaryDB.prototype.updateWordNode = function (word) {

    var me = this;

    var transaction = me.db.transaction([me.dbAndTableName], "readwrite");
    var store = transaction.objectStore(me.dbAndTableName);

    var request = store.put(word);

    request.onsuccess = function (e) {

        console.log("Word updated successfully!");

        return e.target.result;
    };

    request.onerror = function (e) {
        console.log("Could not updated word !");
    };
};

EfficientDictionaryDB.prototype.getAllWordsNames = function () {

    var me = this;

    var words = [];

    var transaction = me.db.transaction([me.dbAndTableName], "readwrite");
    var store = transaction.objectStore(me.dbAndTableName);
    var cursor = store.openCursor();

    cursor.onsuccess = function (e) {

        var results = e.target.result;

        if (results) {

            words.push(results.key);

            results.continue();
        }
    };
};

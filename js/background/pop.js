var db;

var dbAndTableName = "dictionary";

var currentMainWordNode = null;

var turkishWord;

var contentDiv;


function initDB() {

    if (!("indexedDB" in window)) {
        console.log("IndexedDB not supported!");
    }

    var openRequest = indexedDB.open(dbAndTableName, 1);

    openRequest.onupgradeneeded = function (e) {
        var thisDB = e.target.result;

        if (!thisDB.objectStoreNames.contains(dbAndTableName)) {
            thisDB.createObjectStore(dbAndTableName, {keyPath: "word"});
        }
    };

    openRequest.onsuccess = function (e) {

        console.log("DB Opened");

        // Do the magic here !
        db = e.target.result;

    };

    openRequest.onerror = function (e) {
        console.log("Could NOT Opened DB!");
    };
}

function addWord(wordNode) {

    var transaction = db.transaction([dbAndTableName], "readwrite");
    var store = transaction.objectStore(dbAndTableName);

    var request = store.add(wordNode);

    request.onsuccess = function (e) {

        console.log("Word added to DB succesfully!");

        currentMainWordNode = wordNode;

        chrome.tabs.query({active: true, windowId: chrome.windows.WINDOW_ID_CURRENT},
            function (tab) {
                highlightPage(tab[0].id);
            });
    };

    request.onerror = function (e) {
        console.log("Could NOT added to DB !");
        currentMainWordNode = null;
    };
}
function updateWord(wordNode) {

    var transaction = db.transaction([dbAndTableName], "readwrite");
    var store = transaction.objectStore(dbAndTableName);

    var request = store.put(wordNode);

    request.onsuccess = function (e) {

        console.log("Word updated to DB succesfully!");

        currentMainWordNode = wordNode;
    };

    request.onerror = function (e) {
        console.log("Could NOT updated word to DB !");
        currentMainWordNode = null;
    };
}

function startSearchProcess() {

    var jSearch = $("#searchId");

    var input = jSearch.val();

    input = getFilteredInput(input);

    if (input) {

        jSearch.val(input);
        jSearch.select();

        var transaction = db.transaction([dbAndTableName], "readonly");
        var store = transaction.objectStore(dbAndTableName);

        var request = store.get(input);

        request.onsuccess = function (e) {

            console.log("Word retrieved successfully!");

            var wordNode = e.target.result;

            if (wordNode) {
                currentMainWordNode = wordNode;

                var currentSite = getCurrentSite();

                if (currentSite === "tureng") {

                    var node = wordNode.turengNode;
                    if (node) {
                        turengRenderer(node);
                    } else {
                        callTurengSendNodeToProcessor(input, wordNode);
                    }

                } else if (currentSite === "seslisozluk") {

                    var node = wordNode.sesliSozlukNode;
                    if (node) {
                        sesliSozlukRenderer(node);
                    } else {
                        callSesliSozlukSendNodeToProcessor(input, wordNode);
                    }

                } else {

                    var node = wordNode.dictionaryNode;
                    if (node) {
                        dictionaryRenderer(node);
                    } else {
                        callDictionarySendNodeToProcessor(input, wordNode);
                    }
                }
            } else {
                goToWebSiteAndRetrieveData(input);
            }
        };

        request.onerror = function (e) {

            console.log("Could not find word !");
            goToWebSiteAndRetrieveData(input);
        };
    }
}

function goToWebSiteAndRetrieveData(input) {

    var currentSite = getCurrentSite();

    if (currentSite === "tureng") {
        callTurengSendNodeToProcessor(input);
    } else if (currentSite === "seslisozluk") {
        callSesliSozlukSendNodeToProcessor(input);
    } else {
        callDictionarySendNodeToProcessor(input);
    }
}

function triggerSearchProcessWithWord(word) {
    $("#searchId").val(word);
    startSearchProcess();
    $('body,html,#contentDiv').animate({scrollTop: 0}, 800);
}

function createSearchProcess() {

    var jSearch = $("#searchId");

    jSearch.keypress(function (e) {

        if (e.which == 13) {
            startSearchProcess();
        }
    });

    jSearch.focus(function (e) {
        this.select();
    });

    setTimeout(function () {
        focusSearchBar();
    }, 100);
}

function focusSearchBar() {

    var jSearch = $("#searchId");

    var val = jSearch.val();
    if (!val) {
        jSearch.focus();
    }
}

function callTurengSendNodeToProcessor(word, foundNode) {

    var parser = new TurengParser();
    parser.setWord(word);

    showLoadingPage();

    setTimeout(function () {
        turengProcessor(parser.getResultAsJSON(), foundNode)
    }, 100);
}

function callSesliSozlukSendNodeToProcessor(word, foundNode) {

    var parser = new SesliSozlukParser();
    parser.setWord(word);

    showLoadingPage();

    setTimeout(function () {
        sesliSozlukProcessor(parser.getResultAsJSON(), foundNode);
    }, 100);
}

function callDictionarySendNodeToProcessor(word, foundNode) {

    var parser = new DictionaryParser();
    parser.setWord(word);

    showLoadingPage();

    setTimeout(function () {
        dictionaryProcessor(parser.getResultAsJSON(), foundNode);
    }, 100);
}

function turengProcessor(node, foundNode) {

    var isNoNetWork = node.isNoNetwork;

    var isNotFound = node.isNotFound;

    var suggestions = node.suggestions;

    if (isNoNetWork) {

        hideLoadingPage();
        showNoNetworkMessage();
        currentMainWordNode = null;

    } else if (isNotFound) {

        hideLoadingPage();
        showNotFoundMessage();
        currentMainWordNode = null;

    } else if (suggestions.length > 0) {

        turengSuggestionsRenderer(node);
        hideLoadingPage();
        currentMainWordNode = null;

    } else {

        currentMainWordNode = null;

        if (node.isEnglish) {

            turkishWord = null;

            var word = node.word;
            var newNode = new WordNode();
            newNode.word = word;
            newNode.turengNode = node;


            if (foundNode) {
                foundNode.turengNode = node;
                updateWord(foundNode);
            } else {
                addWord(newNode);
            }
        } else {
            turkishWord = node.word;
        }

        turengRenderer(node);
    }
}

function sesliSozlukProcessor(node, foundNode) {

    var isNoNetWork = node.isNoNetwork;

    var isNotFound = node.isNotFound;

    var suggestions = node.suggestions;

    if (isNoNetWork) {

        hideLoadingPage();
        showNoNetworkMessage();
        currentMainWordNode = null;

    } else if (isNotFound) {

        hideLoadingPage();
        showNotFoundMessage();
        currentMainWordNode = null;

    } else if (suggestions.length > 0) {

        sesliSozlukSuggestionRenderer(node);
        hideLoadingPage();
        currentMainWordNode = null;

    } else {

        currentMainWordNode = null;

        if (node.isEnglish) {

            turkishWord = null;

            var word = node.word;
            var newNode = new WordNode();
            newNode.word = word;
            newNode.sesliSozlukNode = node;


            if (foundNode) {
                foundNode.sesliSozlukNode = node;
                updateWord(foundNode);
            } else {
                addWord(newNode);
            }
        } else {
            turkishWord = node.word;
        }

        sesliSozlukRenderer(node);
    }
}

function dictionaryProcessor(node, foundNode) {

    var isNoNetWork = node.isNoNetwork;

    var isNotFound = node.isNotFound;

    if (isNoNetWork) {

        hideLoadingPage();
        showNoNetworkMessage();
        currentMainWordNode = null;

    } else if (isNotFound) {

        hideLoadingPage();
        showNotFoundMessage();
        currentMainWordNode = null;

    } else {

        //isEnglish is default !! true
        var word = node.word;
        var newNode = new WordNode();
        newNode.word = word;
        newNode.dictionaryNode = node;

        currentMainWordNode = null;

        turkishWord = null;

        if (foundNode) {
            foundNode.dictionaryNode = node;
            updateWord(foundNode);
        } else {
            addWord(newNode);
        }

        dictionaryRenderer(node);
    }
}

function abstractRenderer(node) {

    contentDiv = $("#contentDiv");

    contentDiv.empty();

    var icon = (isCurrentWordHighlightActive()) ? "minus" : "plus";

    var wordDiv = '<div id="wordDiv">' +
        '</div><ul id="wordListViewId" data-role="listview" data-split-icon="' + icon + '"' + ' data-split-theme="a" data-inset="true" style="height:15px;width:350px;margin-left: auto;margin-right: auto;">' +
        '<li>' +
        '<a href="#"><h3 style="text-align: center;">' + node.word + '</h3></a>' +
        '<a id="wordRemoveId" data-rel="popup" data-position-to="window" data-transition="pop"></a>' +
        '</li>' +
        '</ul>' +
        '</div>' +
        '<br>' +
        '<br>';

    if (!node.isEnglish) {
        wordDiv += "<br>";
    }
    var hasSoundPaths = !($.isEmptyObject(node.soundsPaths));

    if (!hasSoundPaths) {
        wordDiv += "<br>";
    }

    contentDiv.append(wordDiv);

    if (node.isEnglish) {

        $("#wordRemoveId").on("click", function () {

            if (currentMainWordNode) {

                currentMainWordNode.isHighlightActive = !(currentMainWordNode.isHighlightActive);

                var transaction = db.transaction([dbAndTableName], "readwrite");
                var store = transaction.objectStore(dbAndTableName);

                var request = store.put(currentMainWordNode);

                request.onsuccess = function (e) {

                    $("#wordRemoveId").buttonMarkup({
                        icon: (isCurrentWordHighlightActive()) ? "minus" : "plus"
                    });

                    chrome.tabs.query({active: true, windowId: chrome.windows.WINDOW_ID_CURRENT},
                        function (tab) {
                            highlightPage(tab[0].id);
                        });

                    console.log("Add remove icon updated!");
                };

                request.onerror = function (e) {
                    console.log("Add remove icon could NOT updated!");
                };

            }
        });
    }

    if (hasSoundPaths) {

        var soundDiv = '<div id="soundDiv">' +
            '<table class="soundTable" style="border-spacing: 10px;margin-left: auto;margin-right: auto">' +
            '<tr id="soundTrId">' +
            '</tr>' +
            '</table>' +
            '</div>';

        contentDiv.append(soundDiv);

        var tr = $("#soundTrId");

        var usSound = node.soundsPaths["us"];
        var ukSound = node.soundsPaths["uk"];
        var auSound = node.soundsPaths["au"];

        if (usSound) {

            var usTd = '<td>' +
                '<a id="usOnButtonId" style="font-size: 1px" data-role="button" class="usOnButton" data-corners="true" data-mini="false" data-theme="a" tabIndex="2"></a>' +
                '<audio id="usAudioId"></audio>' +
                '</td>';

            tr.append(usTd);

            createAudioPlayer("usOnButtonId", usSound);
        }

        if (ukSound) {

            var ukTd = '<td>' +
                '<a id="ukOnButtonId" style="font-size: 1px" data-role="button" class="ukOnButton" data-corners="true" data-mini="false" data-theme="a" tabIndex="2"></a>' +
                '<audio id="ukAudioId"></audio>' +
                '</td>';

            tr.append(ukTd);

            createAudioPlayer("ukOnButtonId", ukSound);
        }

        if (auSound) {

            var auTd = '<td>' +
                '<a id="auOnButtonId" style="font-size: 1px" data-role="button" class="auOnButton" data-corners="true" data-mini="false" data-theme="a" tabIndex="2"></a>' +
                '<audio id="auAudioId"></audio>' +
                '</td>';

            tr.append(auTd);

            createAudioPlayer("auOnButtonId", auSound);
        }
    }

    var preSavedMeanings = '<div id="preSavedMeaningsDiv">' +
        '<ul id="preSavedMeaningsListId" data-role="listview" data-split-icon="minus" data-split-theme="a" data-inset="true">' +
        '</ul>' +
        '</div>';

    contentDiv.append(preSavedMeanings);

    var resultDiv = '<div id="resultDiv">' +
        '<ul id="savedListViewId" data-role="listview" data-split-icon="minus" data-split-theme="a" data-inset="true">' +
        '</ul>' +
        '<ul id="listViewId" data-role="listview" data-split-icon="plus" data-split-theme="a" data-inset="true">' +
        '</ul>' +
        '</div>';

    contentDiv.append(resultDiv);
}

function turengRenderer(node) {

    abstractRenderer(node);

    var listViewMain = $("#listViewId");

    var meanings = node.meanings;

    var category = "Try Me Bitch!!!";

    for (var i = 0; i < meanings.length; i++) {

        var itrCategory = meanings[i].category;
        var itrMeaning = meanings[i].meaning;
        var itrType = meanings[i].type;

        if (category !== itrCategory) {
            category = itrCategory;
            listViewMain.append('<li id="dividerLiId-"' + i + ' data-role="list-divider">' + itrCategory + '</li>');
        }

        listViewMain.append('<li><a word="' + itrMeaning + '"' + ' id="meaningLi-' + i + '"' + ' href="#"><h6>' + getFormattedType(itrType) + itrMeaning + '</h6></a><a word="' + itrMeaning + '"' + ' id="meaningLiAdd-' + i + '"' + ' data-rel="popup" data-position-to="window" data-transition="pop"></a></li>');

        $("#meaningLi-" + i).on("click", function () {

            var a = $(this).attr("word");
            triggerSearchProcessWithWord(a);
        });

        $("#meaningLiAdd-" + i).on("click", function () {

            var w = $(this).attr("word");

            addToSavedMeanings(w);
        });
    }

    contentDiv.trigger("create");

    if (node.isEnglish) {
        renderFirstDataSavedMeaningSection(node.word);
    }

    hideLoadingPage();
}

function turengSuggestionsRenderer(node) {

    var contentDiv = $("#contentDiv");
    contentDiv.empty();

    var resultDiv = '<div id="resultDiv">' +
        '<ul id="savedListViewId" data-role="listview" data-split-icon="minus" data-split-theme="a" data-inset="true">' +
        '</ul>' +
        '<ul id="listViewId" data-role="listview" data-split-icon="plus" data-split-theme="a" data-inset="true">' +
        '</ul>' +
        '</div>';

    contentDiv.append(resultDiv);

    var listViewMain = $("#listViewId");

    listViewMain.append('<li id="dividerSuggestion" data-role="list-divider">Öneriler</li>');

    var suggestions = node.suggestions;

    for (var i = 0; i < suggestions.length; i++) {

        listViewMain.append('<li><a word="' + suggestions[i] + '"' + ' id="meaningLi-' + i + '"' + ' href="#"><h6>' + suggestions[i] + '</h6></a></li>');

        $("#meaningLi-" + i).on("click", function () {

            var a = $(this).attr("word");
            triggerSearchProcessWithWord(a);
        });
    }

    contentDiv.trigger("create");
}

function sesliSozlukRenderer(node) {

    abstractRenderer(node);

    //code goes here..
    var listViewMain = $("#listViewId");

    var meanings = node.meanings;

    listViewMain.append('<li id="dividerLiId-"' + i + ' data-role="list-divider">Meanings</li>');

    for (var i = 0; i < meanings.length; i++) {

        var meaning = meanings[i].meaning;
        var category = meanings[i].category;

        listViewMain.append('<li><a word="' + meaning + '"' + ' id="meaningLi-' + i + '"' + ' href="#"><h6>' + meaning + getFormattedCategoryForSesliSozluk(category) + ' </h6></a><a word="' + meaning + '"' + ' id="meaningLiAdd-' + i + '"' + ' data-rel="popup" data-position-to="window" data-transition="pop"></a></li>');

        $("#meaningLi-" + i).on("click", function () {

            var a = $(this).attr("word");
            triggerSearchProcessWithWord(a);
        });

        $("#meaningLiAdd-" + i).on("click", function () {

            var w = $(this).attr("word");

            addToSavedMeanings(w);
        });
    }

    var similarwords = node.similarWords;
    if (similarwords && similarwords.length > 0) {

        listViewMain.append('<li id="dividerLiId-"' + i + ' data-role="list-divider">Benzer kelimeler</li>');

        for (var i = 0; i < similarwords.length; i++) {

            var similarword = similarwords[i];

            listViewMain.append('<li><a word="' + similarword + '"' + ' id="meaningSimilarLi-' + i + '"' + ' href="#"><h6>' + similarword + '</h6></a></li>');

            $("#meaningSimilarLi-" + i).on("click", function () {

                var a = $(this).attr("word");
                triggerSearchProcessWithWord(a);
            });
        }
    }

    contentDiv.trigger("create");

    if (node.isEnglish) {
        renderFirstDataSavedMeaningSection(node.word);
    }

    hideLoadingPage();
}

function sesliSozlukSuggestionRenderer(node) {
    turengSuggestionsRenderer(node);
}

function dictionaryRenderer(node) {

    abstractRenderer(node);

    var mainContdiv = '<div id="mainContId" data-role="main" class="ui-content"></div>';

    contentDiv.append(mainContdiv);

    var mainCont = $("#mainContId");

    var meanings = node.meanings;

    for (var i = 0; i < meanings.length; i++) {

        var meaning = meanings[i];
        mainCont.append('<p style="font-style: italic"><b>' + (i + 1) + ') </b>' + meaning + '</p><br>');
    }

    contentDiv.trigger("create");

    if (node.isEnglish) {
        renderFirstDataSavedMeaningSection(node.word);
    }

    hideLoadingPage();
}

function addToSavedMeanings(word) {

    if (currentMainWordNode) {

        var savedMeanings = currentMainWordNode.savedMeanings;

        if (savedMeanings) {

            if (!(savedMeanings.contains(word))) {

                currentMainWordNode.savedMeanings.push(word);

                var transaction = db.transaction([dbAndTableName], "readwrite");
                var store = transaction.objectStore(dbAndTableName);

                var request = store.put(currentMainWordNode);

                request.onsuccess = function (e) {

                    renderSavedMeaningSection();
                    console.log("Saved meanings updated  succesfully!");

                    chrome.tabs.query({active: true, windowId: chrome.windows.WINDOW_ID_CURRENT},
                        function (tab) {
                            highlightPage(tab[0].id);
                        });
                };

                request.onerror = function (e) {
                    console.log("Saved meanings could NOT updated!");
                };
            }
        }
    }
}

function renderSavedMeaningSection() {

    if (currentMainWordNode) {

        var savedMeanings = currentMainWordNode.savedMeanings;

        if (savedMeanings) {

            var savedUl = $("#savedListViewId");

            savedUl.empty();

            if (savedMeanings.length > 0) {

                savedUl.append('<li id="savedDividerLiId" data-role="list-divider" style="color: #3388cc;font-style: italic;font-size: 16px">~ Saved Meanings ~</li>');

                for (var i = 0; i < savedMeanings.length; i++) {

                    var m = savedMeanings[i];

                    var a = '<li><a word="' + m + '"' + ' id="savedMeaningLi-' + i + '"' + ' href="#"><h6>' + m + '</h6></a><a word="' + m + '"' + ' id="savedMeaningLiRemove-' + i + '"' + ' data-rel="popup" data-position-to="window" data-transition="pop"></a></li>';
                    savedUl.append(a);

                    $("#savedMeaningLi-" + i).on("click", function () {

                        var a = $(this).attr("word");

                        triggerSearchProcessWithWord(a);
                    });

                    $("#savedMeaningLiRemove-" + i).on("click", function () {

                        var a = $(this).attr("word");

                        var index = currentMainWordNode.savedMeanings.indexOf(a);

                        currentMainWordNode.savedMeanings.splice(index, 1);

                        var transaction = db.transaction([dbAndTableName], "readwrite");
                        var store = transaction.objectStore(dbAndTableName);

                        var request = store.put(currentMainWordNode);

                        request.onsuccess = function (e) {

                            renderSavedMeaningSection();
                            console.log("Saved meanings remove updated  succesfully!");
                        };

                        request.onerror = function (e) {
                            console.log("Saved meanings remove could NOT updated!");
                        };
                    });
                }

                savedUl.listview('refresh');
            }
        }
    }
}

function renderFirstDataSavedMeaningSection(word) {

    if (!currentMainWordNode) {

        var transaction = db.transaction([dbAndTableName], "readonly");
        var store = transaction.objectStore(dbAndTableName);

        var request = store.get(word);

        request.onsuccess = function (e) {

            console.log("Word retrieved successfully!");

            currentMainWordNode = e.target.result;

            renderSavedMeaningSection();
        };

        request.onerror = function (e) {
            console.log("Could not find word !");
            currentMainWordNode = null;
        };
    } else {
        renderSavedMeaningSection();
    }
}


function createAudioPlayer(buttonId, soundPath) {

    $("#" + buttonId).on("click", function (e) {

        try {
            var audio = new Audio(soundPath);
            audio.play();
        } catch (e) {
            showNoNetworkMessage();
        }
    });
}

function getFormattedType(type) {

    if (type) {
        return "(" + type + ") ";
    }

    return "";
}

function getFormattedCategoryForSesliSozluk(type) {

    if (type) {
        return " ~ (" + type + ") ";
    }

    return "";
}

function isCurrentWordHighlightActive() {

    if (currentMainWordNode) {
        return currentMainWordNode.isHighlightActive;
    } else {
        return true;
    }
}

function showLoadingPage() {
    $("body").addClass("loading");
}

function hideLoadingPage() {

    setTimeout(function () {
        $("body").removeClass("loading");
    }, 100);
}

function showNoNetworkMessage() {

    $("#noNetConnection").popup("open");

    setTimeout(function () {
        $("#noNetConnection").popup("close");
    }, 2500);
}

function showNotFoundMessage() {

    $("#notFound").popup("open");

    setTimeout(function () {
        $("#notFound").popup("close");
    }, 3000);
}

function showImportHasFinished() {

    $("#importWords").popup("open");

    setTimeout(function () {
        $("#importWords").popup("close");
    }, 2500);
}

function isSwitchOn() {

    var flipswitch = localStorage.getItem("switchKey");

    if (flipswitch && flipswitch === "on") {
        return true;
    }

    return !flipswitch;
}

function getCurrentSite() {

    var language = localStorage.getItem("languageKey");

    if (!language) {
        return "tureng";
    }

    return language;
}

function renderFlipswitchToogle() {

    var flipswitch = $("#switchId");

    flipswitch.on("change", function () {

        var val = $(this).val();

        localStorage.setItem("switchKey", val);

        if (val === "on") {
            //console.log("on beyb!");
            chrome.tabs.query({active: true, windowId: chrome.windows.WINDOW_ID_CURRENT},
                function (tab) {
                    highlightPage(tab[0].id);
                });
        } else {
            //console.log("off beyb!");

            chrome.tabs.query({active: true, windowId: chrome.windows.WINDOW_ID_CURRENT},
                function (tab) {

                    chrome.tabs.sendMessage(tab[0].id, {type: 'switchOff'}, function (response) {
                    });
                });
        }
    });

    var storageVal = localStorage.getItem("switchKey");

    if (!storageVal) {
        localStorage.setItem("switchKey", "on");
        storageVal = "on";
    }

    if (storageVal === "on") {
        flipswitch.val("on").flipswitch("refresh");
    } else {
        flipswitch.val("off").flipswitch("refresh");
    }
}

function renderNavbarLanguage() {

    var tureng = $("#turengId");
    var sesliSozluk = $("#sesliSozlukId");
    var dictionary = $("#dictionaryId");

    tureng.on("click", function () {
        localStorage.setItem("languageKey", "tureng");
        triggerSearchProcessIfWordNodeExists();
    });

    sesliSozluk.on("click", function () {
        localStorage.setItem("languageKey", "seslisozluk");
        triggerSearchProcessIfWordNodeExists();
    });

    dictionary.on("click", function () {
        localStorage.setItem("languageKey", "dictionary");
        triggerSearchProcessIfWordNodeExists();
    });

    var language = localStorage.getItem("languageKey");

    switch (language) {

        case "tureng":

            sesliSozluk.removeClass('ui-btn-hover-b').addClass('ui-btn-up-b');
            dictionary.removeClass('ui-btn-hover-b').addClass('ui-btn-up-b');
            tureng.addClass('ui-btn-active');
            break;

        case "seslisozluk":

            dictionary.removeClass('ui-btn-hover-b').addClass('ui-btn-up-b');
            tureng.removeClass('ui-btn-hover-b').addClass('ui-btn-up-b');
            sesliSozluk.addClass('ui-btn-active');
            break;

        case "dictionary":

            sesliSozluk.removeClass('ui-btn-hover-b').addClass('ui-btn-up-b');
            tureng.removeClass('ui-btn-hover-b').addClass('ui-btn-up-b');
            dictionary.addClass('ui-btn-active');
            break;

        default:

            localStorage.setItem("languageKey", "tureng");

            sesliSozluk.removeClass('ui-btn-hover-b').addClass('ui-btn-up-b');
            dictionary.removeClass('ui-btn-hover-b').addClass('ui-btn-up-b');
            tureng.addClass('ui-btn-active');
            break;
    }
}

function triggerSearchProcessIfWordNodeExists() {

    if (currentMainWordNode && currentMainWordNode.word) {
        triggerSearchProcessWithWord(currentMainWordNode.word);
    } else if (turkishWord) {
        triggerSearchProcessWithWord(turkishWord);
    } else {

        contentDiv = $("#contentDiv");

        contentDiv.empty();

        contentDiv.trigger("create");
    }

    focusSearchBar();
}

document.addEventListener('DOMContentLoaded', function () {

    //console.log("Dom Loaded!");

    initDB();

    //TODO maybe db init could not ready !!
    createSearchProcess();

    chrome.tabs.query({active: true, windowId: chrome.windows.WINDOW_ID_CURRENT},
        function (tab) {


            chrome.tabs.sendMessage(tab[0].id, {method: "getSelection"},
                function (response) {

                    var selectedTex = response.data;
                    if (selectedTex.length > 1) {

                        triggerSearchProcessWithWord(selectedTex);
                    }
                });
        });

    bindImportAndExportEvents();

    renderFlipswitchToogle();

    renderNavbarLanguage();
});

var clickedCount = 0;

function bindImportAndExportEvents() {

    $("#importWordsId").on("click", function () {

        var selectedFile = $('#fileInputId')[0].files[0];


        var reader = new FileReader();

        reader.onload = function (e) {

            var text = reader.result;

            var nodeArray = JSON.parse(text);

            loadWordsToDB(nodeArray);
        };

        if (selectedFile) {
            reader.readAsText(selectedFile, "UTF-8");
        }
        console.log("import clicked");
    });

    $("#exportWordsId").on("click", function () {

        var me = $("#exportWordsIdRef");

        download(me);

        console.log("export clicked");
    });
}

function loadWordsToDB(nodeArray) {

    for (var i = 0; i < nodeArray.length; i++) {
        addWord(nodeArray[i]);
    }

    showImportHasFinished();
}

function download(me) {

    getAllItems(function (items) {

        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        me.attr('download', "Etkili_Sozluk_DB_" + day + "-" + month + "-" + year + ".json");
        me.attr('href', 'data:application/octet-stream;charset=utf-8;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(items)))));

        if(clickedCount == 0){
            $("#exportWordsIdRef").html('<h3 style="text-align: center;">Export Words</h3>');
        }

        clickedCount++;
    });
}

chrome.tabs.onUpdated.addListener(function (tabId, change, tab) {

    if (isSwitchOn()) {

        var url = tab.url;
        if (url !== undefined && change.status == "complete") {
            highlightPage(tab.id);
        }
    }
});

function highlightPage(tabId) {


    if (isSwitchOn()) {


        if (!("indexedDB" in window)) {
            console.log("IndexedDB not supported!");
        }

        var openRequest = indexedDB.open(dbAndTableName, 1);

        openRequest.onupgradeneeded = function (e) {
            var thisDB = e.target.result;

            if (!thisDB.objectStoreNames.contains(dbAndTableName)) {
                thisDB.createObjectStore(dbAndTableName, {keyPath: "word"});
            }
        };

        openRequest.onsuccess = function (e) {

            console.log("DB Opened");

            db = e.target.result;

            getAllHiglihtActiveItems(function (items) {

                var words = [];

                for (var key in items) {

                    if (items.hasOwnProperty(key) && key.split(/\s+/).length === 1) {
                        words.push(key);
                    }
                }

                words = words.getSortedDesc();

                var str = words.join(',');

                chrome.tabs.sendMessage(tabId, {type: 'highlight', wordsString: str, allNodes: items, currentSite: getCurrentSite()}, function (response) {
                });
            });

        };

        openRequest.onerror = function (e) {
            console.log("Could NOT Opened DB!");
        };

    }
}

function getAllHiglihtActiveItems(callback) {
    var trans = db.transaction([dbAndTableName], "readonly");
    var store = trans.objectStore(dbAndTableName);
    var items = {};

    trans.oncomplete = function (evt) {
        callback(items);
    };

    var cursorRequest = store.openCursor();

    cursorRequest.onerror = function (error) {
        console.log(error);
    };

    cursorRequest.onsuccess = function (evt) {

        var cursor = evt.target.result;

        if (cursor) {

            var node = cursor.value;
            if (node.isHighlightActive) {
                items[cursor.key] = node;
            }
            cursor.continue();
        }
    };
}

function getAllItems(callback) {
    var trans = db.transaction([dbAndTableName], "readonly");
    var store = trans.objectStore(dbAndTableName);
    var items = [];

    trans.oncomplete = function (evt) {
        callback(items);
    };

    var cursorRequest = store.openCursor();

    cursorRequest.onerror = function (error) {
        console.log(error);
    };

    cursorRequest.onsuccess = function (evt) {

        var cursor = evt.target.result;

        if (cursor) {

            var node = cursor.value;

            items.push(node);

            cursor.continue();
        }
    };
}

function initAndGetDB(callback) {

    if (!("indexedDB" in window)) {
        console.log("IndexedDB not supported!");
    }

    var openRequest = indexedDB.open(dbAndTableName, 1);

    openRequest.onupgradeneeded = function (e) {
        var thisDB = e.target.result;

        if (!thisDB.objectStoreNames.contains(dbAndTableName)) {
            thisDB.createObjectStore(dbAndTableName, {keyPath: "word"});
        }
    };

    openRequest.onsuccess = function (e) {

        console.log("DB Opened");

        // Do the magic here !
        db = e.target.result;
        callback(db);
    };

    openRequest.onerror = function (e) {
        console.log("Could NOT Opened DB!");
    };
}


Array.prototype.getSortedDesc = function () {
    return this.sort(function (a, b) {
        return b - a;
    })
};


chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {

    updateHiglightOff(message.blinkWordOff);
    console.log(message);
});

function updateHiglightOff(word) {

    initAndGetDB(function (db) {

        var transaction = db.transaction([dbAndTableName], "readwrite");
        var store = transaction.objectStore(dbAndTableName);

        var request = store.get(word);

        request.onsuccess = function (e) {

            console.log("Word retrieved successfully!");

            var wordNode = e.target.result;
            wordNode.isHighlightActive = false;

            var putRequest = store.put(wordNode);

            putRequest.onsuccess = function (e) {
                chrome.tabs.query({active: true, windowId: chrome.windows.WINDOW_ID_CURRENT},
                    function (tab) {
                        highlightPage(tab[0].id);
                    });
            };
        };
    });
}
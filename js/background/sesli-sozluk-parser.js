function SesliSozlukParser() {
    this.word = null;
    this.meanings = [];
    this.suggestions = [];
    this.similarWords = [];
    this.isEnglish = false;
    this.isNotFound = false;
    this.isNoNetwork = false;
    this.soundsPaths = {};
}

SesliSozlukParser.prototype.setWord = function (word) {
    this.word = word;
};

SesliSozlukParser.prototype.getResultAsJSON = function () {

    var me = this;

    $.ajaxSetup({async: false});

    $.get("https://www.seslisozluk.net/?word=" + me.word, function (data) {

        var html = data;

        var parser = new DOMParser();
        var dom = parser.parseFromString(html, "text/html");

        var meanings = dom.getElementsByClassName("panel-body sozluk")[0];
        var suggestions = dom.getElementsByClassName("list-unstyled");

        var isSuggestion = suggestions.length == 3;

        if (meanings) {

            var lang = dom.getElementsByClassName("panel-title text-white font-narrow-uppercase")[0].innerText.trim();

            me.isEnglish = (lang == "İngilizce - Türkçe");

            meanings = meanings.children[0];

            var liList = meanings.children;

            var liLen = liList.length;

            var similarWordSet = new Set();

            for (var i = 0; i < liLen - 1; i++) {

                var li = liList[i];

                if (li.children.length > 0) {

                    if (!hasA(li.children)) {

                        var meaning = new MeaningNode();

                        meaning.setMeaning(getFilteredInput(li.childNodes[0].textContent.trim()));
                        meaning.setCategory(getFilteredInput(li.childNodes[1].textContent.trim()));
                        me.meanings.push(meaning);
                    } else {

                        var similarWord = getSimilarWord(li.children);
                        similarWordSet.add(similarWord);
                    }
                } else {

                    var meaning = new MeaningNode();

                    meaning.setMeaning(getFilteredInput(li.innerText.trim()));
                    me.meanings.push(meaning);
                }
            }

            me.similarWords = similarWordSet.getArray();

            var soundMain = dom.getElementsByClassName("col-xs-3 col-sm-3 col-md-3");

            if (soundMain && me.isEnglish) {

                for (var v = 0; v < 3; v++) {

                    var x = soundMain[v].children[0].children[0].getAttribute("onclick").split(",");

                    var accent = x[0];
                    var mp3Path = x[1];

                    var find = "'";
                    var re = new RegExp(find, 'g');

                    accent = accent.replace(re, '').slice(-2).trim();
                    mp3Path = mp3Path.replace(re, '').trim();
                    mp3Path = "http://www.seslisozluk.net/" + mp3Path;

                    if (accent === "uk") {
                        me.soundsPaths["uk"] = mp3Path;
                    } else if (accent === "us") {
                        me.soundsPaths["us"] = mp3Path;
                    } else if (accent === "au") {
                        me.soundsPaths["au"] = mp3Path;
                    }
                }
            }
        } else if (isSuggestion) {

            var suggestionList = suggestions[0];

            var sugListChildrens = suggestionList.children;

            for (var f = 0; f < sugListChildrens.length; f++) {
                me.suggestions.push(sugListChildrens[f].children[0].innerText.trim());
            }

        } else {
            me.isNotFound = true;
        }

    }).fail(function () {
        me.isNoNetwork = true;
    });

    function hasA(childs) {

        for (var i = 0; i < childs.length; i++) {

            var tagName = childs[i].tagName;

            if (tagName == "A") {
                return true;
            }
        }
    }

    function getSimilarWord(children) {

        for (var i = 0; i < children.length; i++) {

            var tagName = children[i].tagName;

            if (tagName == "A") {
                return getFilteredInput(children[i].innerText.trim());
            }
        }
    }

    return this;
};